import $ from 'jquery';; //this url import only work with webpack
import tablesorter from 'tablesorter';
import { callFetch, dataURL } from '../data/data-covid.js';
import { tableBuild } from '../function/table-build.js';

const tableRender = () => {
    callFetch(dataURL).then(tableWithoutSort => {
        //console.log(tableWithoutSort);
        tableBuild(tableWithoutSort);  
        return tableWithoutSort
    }).then(tableWithSort => {
        $(function() {
            $("#myTable").tablesorter({
              widthFixed: false, 
              dateFormat : "ddmmyyyy", // default date format
              sortList: [[0,1],[0,0]],
              widgets: ['zebra', 'uitheme', 'reflow', 'columnSelector'],
              theme: 'blue',
              widgetOptions : {
                reflow_className : 'tablesorter tablesorter-blue ui-table-reflow',
                
                // target the column selector markup
                columnSelector_container : $('#columnSelector'),
                // data attribute containing column name to use in the selector container
                // make it use the same as reflow_headerAttrib
                columnSelector_name : 'data-name',
          
                // header attribute containing modified header name
                reflow_headerAttrib : 'data-name'
                
              }
            });
          });
          console.log('Tabel telah dimuat...')
        return tableWithSort
    
    })
}



export default tableRender;