import { callFetch, dataURL, formatDate } from '../data/data-covid.js';
import { chartBuild } from '../function/chart-build.js';

const chartRender = () => {
    callFetch(dataURL).then(cleanData => {
        const date = cleanData.map(property => property.Tanggal);
        const trace1 = cleanData.map(property => property.KasusBaruPerHari);
        const trace2 = cleanData.map(property => property.KasusKumulatif);
        const trace3 = cleanData.map(property => property.SembuhPerHari);
        const trace4 = cleanData.map(property => property.SembuhKumulatif);
        const trace5 = cleanData.map(property => property.MeninggalPerHari);
        const trace6 = cleanData.map(property => property.MeninggalKumulatif);

        //target element "option-select" dengan id "myOption" yang ada di dalam webcomponent/shadowroot
        const optionElement = document.querySelector("option-select").shadowRoot.getElementById("myOption"); 
        //console.log(optionElement); <-- untuk cek value option yang dihasilkan component option

        optionElement.addEventListener("change", chartRender);
        let option = optionElement.value
        //debug value ketika option diganti dengan: console.log(optionElement.value);

        //buat fungsi untuk membuat chart dengan data yang berbeda setiap option diganti.
        function showChart(option){
            switch (option) {
                case (option = 'trace1'):
                  return chartBuild(date, trace1);
                case (option = 'trace2'):
                  return chartBuild(date, trace2);
                case (option = 'trace3'):
                  return chartBuild(date, trace3);
                case (option = 'trace4'):
                  return chartBuild(date, trace4);
                case (option = 'trace5'):
                  return chartBuild(date, trace5);
                case (option = 'trace6'):
                  return chartBuild(date, trace6);
            }
        }
        showChart(option);
        console.log('Chart telah dimuat...')
                
    })
};

export default chartRender
