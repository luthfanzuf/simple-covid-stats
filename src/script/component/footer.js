class Footer extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    connectedCallback(){
        this.render();
    }
    render(){
        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }
            :host {
                display: block;
                width: 100%;
                background-color: darkblue;
                color: white;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
            h2 {
                padding: 16px 16px 10px 16px;
                text-align: center;
            }
            p, a {
                color:antiquewhite;
                text-align: center;
                font-size: 12px;
                padding: 3px;
            }
            .footer-content {
                padding-top: 10px;
                padding-bottom: 10px;
            }
        </style>
        <div class="footer-content">
            <p>Sumber Data: <a href='https://apicovid19indonesia-v2.vercel.app/api/indonesia/harian'>https://apicovid19indonesia-v2.vercel.app/api/indonesia/harian</a></p>
            <p>dibuat oleh luthfanzuf<p>
        </div>
        `;
        
    }
}
customElements.define("app-footer", Footer);
