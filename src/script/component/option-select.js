import {$,jQuery} from 'jquery';

//Step by step web-component
//Step 1 menbuat class dengan nama AppBar
class OptionButton extends HTMLElement {
    //step 7 membuat fungsi constructor, super, dan this.shadowDOM
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    
    //Step 2.1 membuat method connectedCallback
    connectedCallback(){
        //Step 3 membuat fungsi this.render didalam connectedCallback
        this.render();
        
    }
    // Step 2.2 membuat fungsi render
    render(){
        //Step 4 vv membuat innerHTML di dalam render beserta HTML-nya
        //Step 8 mengubah this.innerHTML menjadi this.shadowDOM.thisinnerHTML
        //Step 9 membuat styling untuk web-component
        this.shadowDOM.innerHTML = `
        <style>
        #myOption {
            width: 350px;
            font-size: 14px;
        }
        p {
            text-align: left;
            margin: 0;
            padding: 0;
            font-size: 14px;
        }
        @media (min-width: 570px) and (max-width: 800px) {
            #myOption {
                width: 275px;
                font-size: 11px;
            }
            p {
                font-size: 11px;
                text-align: center;
            }
        }
        @media (min-width: 320px) and (max-width: 570px) {
            #myOption {
                width: 200px;
                font-size: 10px;
            }
            p {
                font-size: 10px;
                text-align: center;
            }
        }
        @media (min-width: 150px) and (max-width: 320px) {
            #myOption {
                width: 100px;
                font-size: 9px;
            }
            p {
                font-size: 9px;
                text-align: center;
            }
        }
        </style>
        <p>Pilih data untuk ditampilkan pada grafik :</p>
        <select id="myOption">
            <option value="trace1">Kasus Baru Per Hari</option>
            <option value="trace2">Kasus Kumulatif</option>
            <option value="trace3">Sembuh Per Hari</option>
            <option value="trace4">Sembuh Kumulatif</option>
            <option value="trace5">Meninggal Per Hari</option>
            <option value="trace6">Meninggal Kumulatif</option>
        </select>
        `;
        
    }
    
}
customElements.define("option-select", OptionButton);