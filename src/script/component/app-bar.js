class AppBar extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    connectedCallback(){
        this.render();
    }
    render(){
        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }
            :host {
                display: block;
                width: 100%;
                background-color: darkblue;
                color: white;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
            h2 {
                padding: 16px 16px 16px 16px;
                text-align: center;
            }
            @media (min-width: 570px) and (max-width: 800px) {
                h2 {
                    font-size: 1.17em;
                }
            }
            @media (min-width: 320px) and (max-width: 570px) {
                h2 {
                    font-size: 1em;
                }
            }
            @media (min-width: 150px) and (max-width: 320px) {
                h2 {
                    font-size: 0.83em;
                }
            }
        </style>
        <div>
            <h2>Statistik COVID-19 Indonesia</h2>
        </div>
        `;
        
    }
}
customElements.define("app-bar", AppBar);
