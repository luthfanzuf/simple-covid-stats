class Loading extends HTMLElement {
    constructor() {
        super();
        this.shadowDOM = this.attachShadow({mode: "open"});
    }
    connectedCallback(){
        this.render();
    }
    render(){
        this.shadowDOM.innerHTML = `
        <style>
            * {
                margin: 0;
                padding : 0;
                box-sizing: border-box
            }
            :host {
                display: block;
                width: 100%;
                background-color: navy;
                color: white;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            }
            #initial-overlay {
                position: fixed;
                bottom: 0;
                right: 0;
                left: 0;
                top: 0;
                z-index: 2;
              }
            
            #initial-text{
                color:antiquewhite;
                padding-top: 50px;
                text-align: center;
                font-size: xx-large;
            }
            
            .dark {
                background: darkslategrey;
            }
            
            .exit {
                /* define goals/end state below */
                visibility: hidden;
                opacity: 0;
            
                /* define transition speed, timing, mode, etc. below */
                transition: visibility 0s linear 2s, opacity 2s;
            }
            
            .center-x-y {
                position: absolute;
                left: 50%;
                top: 50%; 
                transform: translate(-50%, -50%);
            }
            
            .center-x {
                position: absolute;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            
            .ml-mr-auto {
                margin-left: auto;
                margin-right: auto;
            }
            
            @media only screen and (max-width: 700px) {
                #initial-text{
                    font-size: x-large;
                    padding-top: 35px;
                }
            
                .spinner {
                    max-width: 60px;
                    height: auto,
                }
            }
            
            @media only screen and (max-width: 500px) {
                #initial-text{
                    font-size: large;
                    padding-top: 20px;
                }
                .spinner {
                    max-width: 30px;
                    height: auto,
                }
            }
        </style>
        <div id="initial-overlay" class="dark" >
            <div class="center-x-y">
                <svg class="spinner center-x" xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="64px" height="64px" viewBox="0 0 128 128" xml:space="preserve"><g><circle cx="16" cy="64" r="16" fill="#faebd7" fill-opacity="1"/><circle cx="16" cy="64" r="16" fill="#fcf2e4" fill-opacity="0.67" transform="rotate(45,64,64)"/><circle cx="16" cy="64" r="16" fill="#fdf7ee" fill-opacity="0.42" transform="rotate(90,64,64)"/><circle cx="16" cy="64" r="16" fill="#fefbf7" fill-opacity="0.2" transform="rotate(135,64,64)"/><circle cx="16" cy="64" r="16" fill="#fefdfa" fill-opacity="0.12" transform="rotate(180,64,64)"/><circle cx="16" cy="64" r="16" fill="#fefdfa" fill-opacity="0.12" transform="rotate(225,64,64)"/><circle cx="16" cy="64" r="16" fill="#fefdfa" fill-opacity="0.12" transform="rotate(270,64,64)"/><circle cx="16" cy="64" r="16" fill="#fefdfa" fill-opacity="0.12" transform="rotate(315,64,64)"/><animateTransform attributeName="transform" type="rotate" values="45 64 64;90 64 64;135 64 64;180 64 64;225 64 64;270 64 64;315 64 64;0 64 64" calcMode="discrete" dur="720ms" repeatCount="indefinite"></animateTransform></g></svg>
                <h2 id="initial-text" class="">mohon tunggu sebentar.</h2>
            </div>
        </div>
        `;
        
    }
}
customElements.define("app-loading", Loading);
