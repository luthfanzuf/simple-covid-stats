function tableBuild(array) { //this function will work with array of objects
    
      // you can use this to turn object key into <thead> data in array form
      //const theadData = Object.keys(Object.assign({}, ...array));
      
      //use this to define theadData manually
      const theadData = ['Tanggal', 'Kasus Baru Perhari', 'Kasus Kumulatif', 'Sembuh Perhari', 'Sembuh Kumulatif', 'Meninggal Perhari', 'Meninggal Kumulatif']

      //getting all value in array of array form        
      const arrays = array.map(Object.values);

      //flatten or merge array of array into single array
      const tbodyData = [].concat.apply([], arrays);

      //define HTML table
      const table = document.createElement("table");

      //define HTML thead & create thead under <table> element
      const tableHeader = table.createTHead();
      const tableBody = table.appendChild(document.createElement('tbody'))

      //define insert row on header & body
      const rowHeader = tableHeader.insertRow();
      let rowBody = tableBody.insertRow();
            
      //define how many column we need
      const columnSize = theadData.length
      //to flag current cell 
      let columnCount = theadData[0].length;

      //add header row
      for (var i of theadData) {
        var headerCell = document.createElement("TH");
        headerCell.innerHTML = theadData[0][i];
        rowHeader.appendChild(headerCell);
        headerCell.innerHTML = i;
      }

      //add body row
      for (var j of tbodyData) {
        var cell = rowBody.insertCell();
        cell.innerHTML = j;
    
        //create new row when columnCount reach columnSize
        columnCount++;
        if (columnCount%columnSize==0) {
          rowBody = tableBody.insertRow();
        }
      }
      const x = document.getElementById("table-container").appendChild(table);
            
      function addAttribute(any) {
        x.setAttribute("id", "myTable");
        x.setAttribute("class", "table tablesorter"); 
      }

      addAttribute();

  }


export {tableBuild}