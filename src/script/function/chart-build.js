import Plotly from 'plotly.js-basic-dist-min' //import plotly

function chartBuild(x, y) {  
  var layout = {
    font: {
      size: 8
    },
    margin: {
      l: 40,
      r: 20,
      b: 60,
      t: 20,
      pad: 4
    }, 
    autosize: true
    
  };
  var config = {
    responsive: true};

  let data = [
        {
          x: (x),
          y: (y),
          type: 'scatter'
        }
      ];
      
      Plotly.newPlot('chart-container', data, layout, config);
};
export {chartBuild}

 