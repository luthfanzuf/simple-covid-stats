import 'jquery'; //this url import only work with webpack

const dataURL = "https://apicovid19indonesia-v2.vercel.app/api/indonesia/harian"

function callFetch(url) {
  //fetch "GET" from URL
  return fetch(url, {
    method: "GET"
  }).then(response => {
    // .then response, return response, create if function to see error
    if(!response.ok) {
      throw Error (`.then response error. something is wrong.`)
    }
    return response.json(); //promise response and return response.json()
  }).then(data => {
    //select and transform the data
    //use console log here to see how the data change
    let data1 = data.data;

    //hilangkan tanggal yang nilainya duplikat
    /*
    let uniqTanggal = Array.from(new Set(data1.map(a => a.tanggal)))
    .map(tanggal => {
      return data1.find(a => a.tanggal === tanggal)
    })*/

    // hilangkan index ke-152 dari array karena duplikat
    //let remove152 = uniqTanggal.splice(152, 1) && uniqTanggal

    //transform to new array of objects with selected properties
    //transformData(data1);
    //let transformedData = transformData(remove152);

    console.log(data);
    let transformedData = transformData(data);
    console.log(transformedData);

    //filter data yang memiliki nilai null
    let cleanData = transformedData.filter(function (entries) {
      return entries.KasusBaruPerHari != null;
    })
    //console.log(filteredData)
    return cleanData
  })  
}

function formatDate (dateString){
  return new Date(dateString).toLocaleDateString('id-ID') //fungsi utuk mengubah format tanggal
};

function transformData(data) {
  return data.map(function(property) {
    // declare a new object
    var newObject = {};
    // disni bisa ditentukan seperti apa properti dari masing-masing key object.
    newObject['Tanggal'] = formatDate(property.tanggal);
    //newObject['Tanggal'] = property.tanggal;
    newObject['KasusBaruPerHari'] = property.positif;
    newObject['KasusKumulatif'] = property.positif_kumulatif;
    newObject['SembuhPerHari'] = property.sembuh;
    newObject['SembuhKumulatif'] = property.sembuh_kumulatif;
    newObject['MeninggalPerHari'] = property.meninggal;
    newObject['MeninggalKumulatif'] = property.meninggal_kumulatif;

    // return our new object.
    return newObject;
  })
};

function removeDuplicate(data){

}

export { callFetch, dataURL, formatDate }