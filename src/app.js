import "regenerator-runtime/runtime.js";
import "./script/component/app-loading.js";
import "./style/style.css";
import "./script/component/app-bar.js";
import "./script/component/option-select.js";
import "./script/component/footer.js";
import tableRender from './script/view/table-view.js';
import chartRender from './script/view/chart-view.js';
import "./style/bootstrap.min.css";
import "./style/theme.blue.min.css";



function loadingEnd () {
    setTimeout(function () {
        document.querySelector("app-loading").shadowRoot.getElementById("initial-overlay").className += " exit";
    }, 6000)
}

async function loadContent() {
    let x = tableRender();
    x = await chartRender();
}

loadContent().then(loadingEnd());
